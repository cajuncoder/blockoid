package blockoid.state;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;

import blockoid.Game;
import blockoid.game.GUI;
import blockoid.game.World;
import blockoid.graphics.Button;
import blockoid.graphics.FontUtil;

public class PauseState extends MenuState {
	
	//public int textWidth = 0;
	public GameState gameState;
	
	public PauseState(Game game) {
		super(game);
		this.TITLE = "PAUSED";
		drawPreviousState = true;
		addOption("resume", "Resume");
		addOption("save", "Save World");
		addOption("load", "Load World");
		addOption("main", "Quit to Main Menu");
	}
	
	public void update(long elapsedTime) {		
		gameState = game.getGameState();
			
		super.update(elapsedTime);
		
		// If a menu option was clicked, change states
		if (isOptionClicked("resume") || game.keyboard.isKeyTyped(KeyEvent.VK_ESCAPE)) {
			game.popState();
		}
		
		if (isOptionClicked("save")) {
			gameState.world.save();
		}
		
		if (isOptionClicked("load")) {
			World newWorld = World.load();
			newWorld.loadTransient(game, gameState);
			gameState.world = newWorld;
			newWorld.update(elapsedTime);
			gameState.gui = new GUI(gameState);
			gameState.gui.update();
		}
		
		if (isOptionClicked("main")) {
			game.resetState(new MainMenuState(game));
		}
		
		
	}
	
	public void draw(Graphics2D g) {
		// Draw Previous State

		if(gameState!=null) {
			gameState.draw(g);
		}
		
		int optionsHeight = getHeight();
		
		// Draw the menu background
		g.setColor(Color.BLACK);
		//g.fillRect(dx, dy, textWidth, optionsHeight);
		
		// Draw the game title
		g.setFont(titleFont);
		g.setColor(Color.WHITE);
		g.drawString(TITLE, game.width/2 - (FontUtil.getWidth(TITLE, titleFont)/2), game.height/6);

		// Draw the menu options
		//int optionsHeight = getHeight();
		int y = game.height/2 - (optionsHeight/2);
		
		for (int o = 0; o < options.size(); o++) {
			int x = game.width/2 - (options.get(o).getWidth()/2);
			options.get(o).setPos(x, y);

			g.setColor(options.get(o).selected ? Color.RED : Color.WHITE);

			//g.setColor(options.get(o).selected ? Color.GREEN : Color.WHITE);
			options.get(o).draw(g);
			y += options.get(o).getHeight();
		}
	}
}
