package blockoid.game.tile;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.Serializable;
import java.lang.reflect.Constructor;

import blockoid.Assets;
import blockoid.audio.Audio;
import blockoid.game.World;
import blockoid.game.item.Item;
import blockoid.game.object.GameObject;
import blockoid.graphics.SpriteSheet;

public abstract class Tile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static int TILE_SIZE = 8;
	public int x;
	public int y;
	public int xIndex;
	public int yIndex;
	public transient SpriteSheet sprite;
	public transient SpriteSheet damageOverlay;
	public double lightLevel = 0;
	public double density = 0.9;
	public boolean inTheSun = false;
	public boolean solid;
	public boolean isBackgroundTile = false;
	public int hitpool = 0;
	public int hitpoints = 0;
	public transient Audio breakSound = null;
	public GameObject object = null;
	
	public double illumination = 0;
	
	public void loadTransient() {
		sprite = null;
		breakSound = null;
		damageOverlay = Assets.getSpriteSheet("tiles/damageOverlay", TILE_SIZE, TILE_SIZE);
		if(this.object!=null) object.loadTransient();
	}
	
	public Tile(int xIndex, int yIndex, boolean isBackgroundTile) {
		this.isBackgroundTile = isBackgroundTile;
		this.xIndex = xIndex;
		this.yIndex = yIndex;
		this.x = xIndex*TILE_SIZE;
		this.y = yIndex*TILE_SIZE;
		this.damageOverlay = Assets.getSpriteSheet("tiles/damageOverlay", TILE_SIZE, TILE_SIZE);
	}
	
	public void update(World world) {
		processHP(world);
	}
	
	public Item getItemDrop() {
		Item item = null;
		return item;
	}
	
	int healCounter = 0;
	public void processHP(World world) {
		if(hitpool > 0) {
			healCounter++;
			if(hitpoints <= 0) {
				destroy(world);
			}
			if(hitpoints>0 && hitpoints < hitpool  && healCounter >= 60) {
				hitpoints++;
				healCounter=0;
			}
		}
	}
	
	public void damage(World world, int value) {
		healCounter=0;
		hitpoints-=value;
	}
	
	public void destroy(World world) {
		if(getItemDrop()!=null)
			world.addItem(getItemDrop().getNewInstance(), x+4, y);

		Tile replacement = new Empty(xIndex,yIndex,isBackgroundTile);
		replacement.lightLevel=this.lightLevel;
		if(!isBackgroundTile) {
			world.tiles[xIndex][yIndex] = replacement;
			if(breakSound!=null) breakSound.play(false);
		}else{
			replacement.isBackgroundTile = true;
			world.bgTiles[xIndex][yIndex] = replacement;
			if(breakSound!=null) breakSound.play(false);
		}
	}
	
	public void getLight(World world) {
		if(yIndex >= world.renderStartY && yIndex <= world.renderEndY) {
			if(xIndex >= world.renderStartX && xIndex <= world.renderEndX) {
	
		//if(world.tiles[xIndex][yIndex].getClass().equals(Empty.class) &&
		//		world.bgTiles[xIndex][yIndex].getClass().equals(Empty.class)) {
		//	this.inTheSun = true;
		//}
		//if(inTheSun && world.sunlightLevel > lightLevel) lightLevel = world.sunlightLevel;
		if(illumination > lightLevel) lightLevel = illumination;
		illumination = 0;
		
		if(lightLevel >= density && density > 0) {

		Tile right = null;
		Tile left = null;
		Tile up = null;
		Tile down = null;
		if(xIndex+1 < world.sizeX-1) right = world.tiles[xIndex+1][yIndex];
		if(xIndex-1 > 0) left = world.tiles[xIndex-1][yIndex];
		if(yIndex-1 > 0) up = world.tiles[xIndex][yIndex-1];
		if(yIndex+1 < world.sizeY-1) down = world.tiles[xIndex][yIndex+1];

		//left
		if(left!=null) {
			if(lightLevel-left.density > left.lightLevel) {
				 left.lightLevel = lightLevel-left.density;
				 //if(this.inTheSun && !left.solid) left.inTheSun=true;
				 left.getLight(world);
			}
		}
		//right
		if(right!=null) {
			if(lightLevel-right.density > right.lightLevel) {
				 right.lightLevel = lightLevel-right.density;
				 //if(this.inTheSun && !right.solid) right.inTheSun=true;
				 right.getLight(world);
			}
		}
		//up
		if(up!=null) {
			if(lightLevel-up.density > up.lightLevel) {
				 up.lightLevel = lightLevel-up.density;
				 up.getLight(world);
			}
		}
		//down
		if(down!=null) {
			if(lightLevel-down.density > down.lightLevel) {
				down.lightLevel = lightLevel-down.density;
				if(this.inTheSun && !down.solid) {
					down.inTheSun=true;
				}
				down.getLight(world);
			}
		}
			}
		}
		}
		if(lightLevel < 0) lightLevel = 0;
		//if(inTheSun && world.sunlightLevel > lightLevel) lightLevel = world.sunlightLevel;
	}
	
	
	public Tile newInstance(int xIndex, int yIndex, boolean isBackgroundTile) {

			Object instance = null;
			Class<?> clazz = this.getClass();
			try {
				Constructor<?> constructor = clazz.getConstructor(
						int.class, int.class, boolean.class);
				instance = constructor.newInstance(xIndex, yIndex, isBackgroundTile);
			} catch (Exception e) {
				e.printStackTrace();
			}
		return (Tile) instance;
	}
	
	public void draw(Graphics2D g, int OffX, int OffY) {
		if(lightLevel>0) {
		sprite.drawSprite(x-OffX, y-OffY, 0, (int)Math.ceil(lightLevel), g);
		drawDamageOverlay(g, OffX, OffY);
		}else{
			g.setColor(Color.BLACK);
			g.fillRect(x-OffX, y-OffY, 8, 8);
		}
	}
	
	public void drawDamageOverlay(Graphics2D g, int OffX, int OffY) {
		if(hitpool>0 && hitpoints < hitpool) {
			if(hitpoints<0) hitpoints=0;
			int multiple = 4;
			if(hitpoints>0) {
				multiple=hitpool/hitpoints;
			}
			int frame = 4/multiple;			
			damageOverlay.drawSprite(x-OffX, y-OffY, -frame+4, (int)Math.ceil(lightLevel), g);
		}
	}
	
	//private double capLight(double i) {
	//	if(i > 8) i=8;
	//	if(i < 0) i=0;
	//	return i;
	//}
	//public void drawLightMask(Graphics2D g, int OffX, int OffY) {
	//	lightMask.drawSprite(x-OffX, y-OffY, (int)Math.ceil(lightLevel), g);
	//}

	public void setLight(int i) {
		illumination = (double) i;
	}
}
