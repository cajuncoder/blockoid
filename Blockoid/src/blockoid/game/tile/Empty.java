package blockoid.game.tile;

import java.awt.Graphics;
import java.awt.Graphics2D;

import blockoid.Assets;
import blockoid.graphics.Sprite;

public class Empty extends Tile {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void loadTransient() {
		sprite = null;
		damageOverlay = null;
		if(this.object!=null) object.loadTransient();
	}
	
	public Empty(int yIndex, int xIndex, boolean isBackgroundTile) {
		super(yIndex, xIndex, isBackgroundTile);
		sprite = null;
		solid = false;
		lightLevel = 0;
		density = 0.5;
		this.damageOverlay=null;
	}

	public void draw(Graphics2D g, int OffX, int OffY) {
		//sprite.drawSprite(x-OffX, y-OffY, 0, g);
	}
	
}

