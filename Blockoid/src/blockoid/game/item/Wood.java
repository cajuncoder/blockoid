package blockoid.game.item;

import java.awt.Graphics2D;

import blockoid.Assets;
import blockoid.game.being.Being;
import blockoid.game.tile.Tile;
import blockoid.graphics.SpriteSheet;

public class Wood extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void loadTransient() {
		inventorySprite = Assets.getSpriteSheet("items/wood", 11, 11);
		handSprite = Assets.getSpriteSheet("characters/playerHands", 16, 16);
	}
	
	public Wood() {
		name = "Wood";
		stackable = true;
		maxInStack = 16;
		inventorySprite = Assets.getSpriteSheet("items/wood", 11, 11);
		handSprite = Assets.getSpriteSheet("characters/playerHands", 16, 16);
	}
	
	public Item getNewInstance() {
		Item result = new Wood();
		return result;
	}
	
	public void drawInHand(Graphics2D g, int OffX, int OffY, Being being) {
		if(handSprite!=null) {
			int dx = being.dx-(being.sprite.spriteSizeX/2)-OffX;
			int dy = being.dy-(being.sprite.spriteSizeY-1)-OffY;
			dy = dy-(handSprite.spriteSizeY-16)/2;
			dx = dx-(handSprite.spriteSizeX-16)/2;
			handSprite.drawSprite(dx, dy, being.frame, being.lightLevel, being.facing, g);
		}
	}
}
