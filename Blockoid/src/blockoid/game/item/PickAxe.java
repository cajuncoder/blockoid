package blockoid.game.item;

import blockoid.Assets;
import blockoid.audio.Audio;
import blockoid.game.World;
import blockoid.game.being.Being;
import blockoid.game.object.GameObject;
import blockoid.game.tile.Empty;
import blockoid.game.tile.Tile;
import blockoid.graphics.SpriteSheet;

public class PickAxe extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void loadTransient() {
		handSprite = Assets.getSpriteSheet("items/usingPickaxe2", 32, 32);
		inventorySprite = Assets.getSpriteSheet("items/inventoryPickaxe", 11, 11);
		pickaxeHit = Assets.getAudio("pickaxeHit");
	}
	public int[] swing;
	public int attackRange = 21;
	public int blockBreakRange = 96;
	public transient Audio pickaxeHit = Assets.getAudio("pickaxeHit");

	public PickAxe() {
		super.name = "PickAxe";
		super.handSprite = Assets.getSpriteSheet("items/usingPickaxe2", 32, 32);
		super.inventorySprite = Assets.getSpriteSheet("items/inventoryPickaxe", 11, 11);
		pickaxeHit = Assets.getAudio("pickaxeHit");
		frame = 0;
		int w = handSprite.spritesX;
		swing = new int[]{0+w, 2+w,3+w,4+w, 4+w, 4+w};
		isTwoHanded = true;
	}
	
	public Item getNewInstance() {
		Item result = new PickAxe();
		return result;
	}
	
	int swingCounter = 0;
	int ticksPerFrame = 4;
	int tick = 0;
	int chamberTime = 0;
	boolean swinging = false;
	boolean oldHold = false;
	boolean oldHold2 = false;
	boolean chambered = false;
	boolean released = false;
	boolean released2 = false;
	
	public void processPrimary(World world, Being weilder) {
		//world.tiles[(int) (weilder.x/8)][(int) (weilder.y/8)-1].lightLevel = 7;
		//world.tiles[(int) (weilder.x/8)][(int) (weilder.y/8)-1].getLight(world);
		boolean hold = false;
		boolean hold2 = false;
		
		//if(weilder.rightHandItem.equals(this) && weilder.leftHandItem.equals(this)) {
			hold = world.game.mouse.holdL;
			hold2 = world.game.mouse.holdR;
		//}else
		//if(weilder.rightHandItem.equals(this)) {
		//	hold = world.game.mouse.holdR;
		//}else
		//if(weilder.leftHandItem.equals(this)) {
		//	hold = world.game.mouse.holdL;
		//}

		
		if(oldHold==true && hold==false) released = true;
		if(oldHold2==true && hold2==false) released2 = true;
		//System.out.println(released);
		if(hold && !swinging || hold2 && !swinging) {
			animation = swing;
			frame = 0;
			chamberTime = 0;
			chambered = false;
			swinging = false;
			crouch = false;
			released = false;
			released2 = false;
		} else if (released || released2 && !swinging){
			swinging = true;
			chamberTime++;
		}
		if(chamberTime>0) chamberTime++;
		if(chamberTime > ticksPerFrame*3) {
			chambered = true;
			chamberTime = 0;
		}
		
		if(swinging && chambered) {
			this.crouch = true;
			tick++;
			if(tick > ticksPerFrame && frame < swing.length) {
				frame++;
				tick = 0;
				checkHit(world, weilder);
				if (frame==swing.length-2) checkWorldHit(world, weilder);
			}
			
			if (frame >= swing.length) {
				animation = null;
				frame = 0;
				tick = 0;
				swinging = false;
				crouch = false;
				chamberTime = 0;
				released = false;
				released2 = false;
			}
		}

		oldHold = hold;
		oldHold2 = hold2;
	}
	
	public void checkWorldHit(World world, Being weilder) {
		GameObject selectedObject = null;
		Tile selectedTile = null;
		
		int tileX = (int) ((world.game.mouseMotion.x+world.CameraOffX)/8);
		int tileY = (int) ((world.game.mouseMotion.y+world.CameraOffY)/8);
		if(tileX >= world.sizeX) tileX = world.sizeX-1;
		if(tileX < 0) tileX = 0;
		if(tileY >= world.sizeY) tileY = world.sizeY-1;
		if(tileY < 0) tileY = 0;
	
		if(released) selectedTile = world.tiles[tileX][tileY];
		if(released2) selectedTile = world.bgTiles[tileX][tileY];
		if(selectedTile.object!=null) {
			selectedObject = selectedTile.object;
		}else if(weilder.equals(world.player)) {
			selectedObject = world.player.selectedObject;
		}
		
		
		if (selectedObject!=null && selectedObject.tile!=null && !selectedObject.tile.isBackgroundTile) {
			selectedObject.damage(world, 2);
			pickaxeHit.play(false);
		} else
		if (!selectedTile.getClass().equals(Empty.class) && selectedTile.object==null) {
			selectedTile.damage(world, 4);
			pickaxeHit.play(false);
		} else if (selectedObject!=null) {
			selectedObject.damage(world, 2);
			pickaxeHit.play(false);
		}
		
		/*
		if(released) {
			if(world.tiles[tileX][tileY].object!=null){
				selectedTile = world.tiles[tileX][tileY];
			world.tiles[tileX][tileY].object.damage(1);
			}else{world.tiles[tileX][tileY].damage(1);}
		}
		else if(released2) {
			if(world.bgTiles[tileX][tileY].object!=null){
			world.bgTiles[tileX][tileY].object.damage(1);
			}else{world.bgTiles[tileX][tileY].damage(1);}
		}

		else {
			selectedObject.damage(1);
			//selectedObject.
		}
		
		*/
	}
	/*
	boolean oldHold2 = false;
	public void processSecondary(World world, Being weilder) {
		boolean hold2 = world.game.mouse.holdR;
		boolean released = false;
		
		if(oldHold2==true && hold2==false) released = true;
		System.out.println(released);
		if(hold2) {
			animation = swing;
			frame = 0;
			chamberTime = 0;
			chambered = false;
			swinging = false;
			crouch = false;
		} else if (released){
			swinging = true;
			chamberTime++;
		}
		if(chamberTime>0) chamberTime++;
		if(chamberTime > ticksPerFrame*2) {
			chambered = true;
			chamberTime = 0;
		}
		
		if(swinging && chambered) {
			this.crouch = true;
			tick++;
			if(tick > ticksPerFrame && frame < swing.length) {
				frame++;
				tick = 0;
				checkHit(world, weilder);
			}
			if (frame >= swing.length) {
				animation = null;
				frame = 0;
				tick = 0;
				swinging = false;
				crouch = false;
				chamberTime = 0;
			}
		}

		oldHold2 = hold2;
	}
	*/
	
	private void checkHit(World world, Being weilder) {
		
		for(Being b : world.beings) {
			int dist = (int) (Math.abs(weilder.x - b.x) + Math.abs(weilder.y - b.y));
			
			if(dist < attackRange) {
				int facingMulti = +1; if(weilder.facing == weilder.LEFT) facingMulti = -1;
				double angleX = (weilder.x - b.x)/dist*facingMulti;
				double angleY = (weilder.y - b.y)/dist;
				if(frame == 1) {
					if(angleY > 0 && angleX > -0.5 && angleX < 0.5) {
						int damage = frame;
						b.hurt(damage, world);
						b.knockBack(weilder, 2);
					}
				}
				
				if(frame >= swing.length-3) {
					if(angleY > 0 && angleX > -1 && angleX < 0.0) {
						int damage = frame;
						b.hurt(damage, world);
						b.knockBack(weilder, 3);
					}
				}

				if(frame == swing.length-2) {
					if(angleX < 0 && angleY > -0.5 && angleY < 0.5) {
						int damage = frame;
						b.hurt(damage, world);
						b.knockBack(weilder, 3);
					}
				}
				
				if(frame == swing.length-1) {
					if(angleX < 0 && angleY > -0.5 && angleY < 0.5) {
						int damage = 0;
						b.hurt(damage, world);
						b.knockBack(weilder, 1);
					}
				}
			}
		}
		
	}


	
}
