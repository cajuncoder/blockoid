package blockoid.game.item;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.Serializable;

import blockoid.Assets;
import blockoid.game.Inventory;
import blockoid.game.World;
import blockoid.game.being.Being;
import blockoid.graphics.SpriteSheet;

public abstract class Item implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public double x;
	public double y;
	public double xVel = 0;
	public double yVel = 0;
	public int lightLevel = 0;
	public String name;
	public transient SpriteSheet inventorySprite;
	public transient SpriteSheet handSprite;
	public Being weilder;
	public int frame = 0;
	public int[] animation = null;
	public boolean stackable = false;
	public boolean crouch = false;
	public boolean isTwoHanded = false;
	public int numInStack = 1;
	public int maxInStack = 16;
	public Inventory inventory = null;
	
	public void loadTransient() {
		// TODO Auto-generated method stub
	}
	
	public void processPrimary(World world, Being weilder) {
		
	}
	
	public void processSecondary(World world, Being weilder) {
		
	}
	
	public void update(World world) {
		int tileX = (int) (x/8);
		int tileY = (int) (y/8);
		if(tileX >= world.sizeX) tileX = world.sizeX-1;
		if(tileX < 0) tileX = 0;
		if(tileY >= world.sizeY) tileY = world.sizeY-1;
		if(tileY < 0) tileY = 0;
		
		lightLevel = (int)Math.ceil(world.tiles[tileX][tileY].lightLevel);
		
		if(!world.tiles[tileX][tileY].solid && yVel < 2.5) {
			yVel+=0.1;
		}
		if(xVel > 0) xVel -= 0.05;
		if(xVel < 0) xVel += 0.05;
		if(xVel > 4) xVel = 4;
		if(xVel < -4) xVel = -4;
		if(yVel > 4) yVel = 4;
		if(yVel < -4) yVel = -4;
		
		y+=yVel;
		x+=xVel;
		
		if(world.tiles[tileX][tileY].solid) {
			y=world.tiles[tileX][tileY].y;
			xVel = 0;
		}
		
		//replace this later
		if(world.player!=null) {
			int distX = (int) Math.abs(world.player.x - x);
			int distY = (int) Math.abs(world.player.y - y);
			if(distX < 8 && distY < 8) {
				if(world.player.toolbelt.canAddItem(this)) {
					world.player.toolbelt.addItem(this);
					world.items.remove(this);
				}else
				if(world.player.inventory.canAddItem(this)) {
					world.player.inventory.addItem(this);
					world.items.remove(this);
				}
			}
		}
	}
	
	public Item getNewInstance() {
		return null;
	}
	
	public void draw(Graphics2D g, int dx, int dy) {
		inventorySprite.drawSprite(dx, dy, 0, g);
		if(stackable && numInStack > 1) {
			g.setColor(Color.WHITE);
			g.drawString(Integer.toString(numInStack), dx+1, dy+8);
		}
	}
	
	public void worldDraw(Graphics2D g, int xOff, int yOff) {
		int dx = (int) x-xOff-(inventorySprite.spriteSizeX/2);
		int dy = (int) y-yOff-inventorySprite.spriteSizeY;
		inventorySprite.drawSprite(dx, dy, 0, lightLevel, g);
	}
	
	public void drawInHand(Graphics2D g, int OffX, int OffY, Being weilder) {
		if(handSprite!=null) {
			int dx = weilder.dx-(weilder.sprite.spriteSizeX/2)-OffX;
			int dy = weilder.dy-(weilder.sprite.spriteSizeY-1)-OffY;
			//int dx = (int) being.dx-xOff-(8);
			//int dy = (int) being.dy-yOff-(8);
			dy = dy-(handSprite.spriteSizeY-16)/2;
			dx = dx-(handSprite.spriteSizeX-16)/2;
			if(animation==null) {
				handSprite.drawSprite(dx, dy, weilder.frame, weilder.lightLevel, weilder.facing, g);
			} else {
				handSprite.drawSprite(dx, dy, animation[frame], weilder.lightLevel, weilder.facing, g);
			}
		}
	}

}
