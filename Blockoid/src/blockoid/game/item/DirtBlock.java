package blockoid.game.item;

import java.awt.Color;
import java.awt.Graphics2D;

import blockoid.Assets;
import blockoid.game.World;
import blockoid.game.tile.Desert;
import blockoid.game.tile.DesertStone;
import blockoid.game.tile.Dirt;
import blockoid.game.tile.Tile;
import blockoid.graphics.SpriteSheet;

public class DirtBlock extends Block {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DirtBlock() {
		this(1);
	}
	
	public void loadTransient() {
		inventorySprite = Assets.getSpriteSheet("tiles/dirt", Tile.TILE_SIZE, Tile.TILE_SIZE);
		handSprite = Assets.getSpriteSheet("characters/playerHands", 16, 16);
	}
	
	public DirtBlock(int num) {
		super(num);
		name = "Dirt Block";
		inventorySprite = Assets.getSpriteSheet("tiles/dirt", Tile.TILE_SIZE, Tile.TILE_SIZE);
		handSprite = Assets.getSpriteSheet("characters/playerHands", 16, 16);
	}
	
	public Item getNewInstance() {
		Item result = new DirtBlock();
		return result;
	}
	
	public Tile getNewTileInstance(int tileX, int tileY, boolean bgTile) {
		Tile tile = new Dirt(tileX,tileY,bgTile);
		return tile;
	}
	
	
}
