package blockoid.game.item;

import blockoid.Assets;
import blockoid.game.tile.DesertStone;
import blockoid.game.tile.Stone;
import blockoid.game.tile.Tile;

public class DesertStoneBlock extends Block {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void loadTransient() {
		inventorySprite = Assets.getSpriteSheet("tiles/desertStone", Tile.TILE_SIZE, Tile.TILE_SIZE);
		handSprite = Assets.getSpriteSheet("characters/playerHands", 16, 16);
	}
	
	public DesertStoneBlock() {
		super(1);
		name = "Sandstone Block";
		inventorySprite = Assets.getSpriteSheet("tiles/desertStone", Tile.TILE_SIZE, Tile.TILE_SIZE);
		handSprite = Assets.getSpriteSheet("characters/playerHands", 16, 16);
	}
	
	public Item getNewInstance() {
		Item result = new DesertStoneBlock();
		return result;
	}
	
	public Tile getNewTileInstance(int tileX, int tileY, boolean bgTile) {
		Tile tile = new DesertStone(tileX,tileY,bgTile);
		return tile;
	}
	
}
