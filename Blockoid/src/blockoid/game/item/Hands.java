package blockoid.game.item;

import java.awt.Color;
import java.awt.Graphics2D;

import blockoid.Assets;
import blockoid.game.World;
import blockoid.game.being.Being;
import blockoid.game.tile.Tile;
import blockoid.graphics.SpriteSheet;

public class Hands extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void loadTransient() {
		handSprite = Assets.getSpriteSheet("characters/playerHands", 16, 16);
		inventorySprite = null;//Assets.getSpriteSheet("items/inventoryPickaxe", 11, 11);
	}
	
	public Hands() {
		super.name = "Empty Hands";
		super.handSprite = Assets.getSpriteSheet("characters/playerHands", 16, 16);
		super.inventorySprite = null; //Assets.getSpriteSheet("items/inventoryPickaxe", 11, 11);
	}
	
	public void draw(Graphics2D g, int dx, int dy) {
	}
	
	public void worldDraw(Graphics2D g, int xOff, int yOff) {
	}
	
	public void processPrimary(World world) {
		//crouch = true;
	}
	
	public void drawInHand(Graphics2D g, int OffX, int OffY, Being being) {
		if(handSprite!=null) {
			int dx = being.dx-(being.sprite.spriteSizeX/2)-OffX;
			int dy = being.dy-(being.sprite.spriteSizeY-1)-OffY;
			dy = dy-(handSprite.spriteSizeY-16)/2;
			dx = dx-(handSprite.spriteSizeX-16)/2;
			handSprite.drawSprite(dx, dy, being.frame, being.lightLevel, being.facing, g);
		}
	}
	
}
