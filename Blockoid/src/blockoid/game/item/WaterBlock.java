package blockoid.game.item;

import blockoid.Assets;
import blockoid.game.tile.Desert;
import blockoid.game.tile.Stone;
import blockoid.game.tile.Tile;
import blockoid.game.tile.Water;

public class WaterBlock extends Block {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void loadTransient() {
		inventorySprite = Assets.getSpriteSheet("tiles/desert", Tile.TILE_SIZE, Tile.TILE_SIZE);
		handSprite = Assets.getSpriteSheet("characters/playerHands", 16, 16);
	}
	
	public WaterBlock() {
		this(1);
	}
	
	public WaterBlock(int num) {
		super(num);
		name = "Water Block";
		inventorySprite = Assets.getSpriteSheet("tiles/desert", Tile.TILE_SIZE, Tile.TILE_SIZE);
		handSprite = Assets.getSpriteSheet("characters/playerHands", 16, 16);
	}
	
	public Item getNewInstance() {
		Item result = new WaterBlock();
		return result;
	}
	
	public Tile getNewTileInstance(int tileX, int tileY, boolean bgTile) {
		Tile tile = new Water(tileX,tileY);
		return tile;
	}
	
}
