package blockoid.game.item;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Random;

import blockoid.Assets;
import blockoid.game.World;
import blockoid.game.being.Being;
import blockoid.game.tile.Tile;
import blockoid.graphics.SpriteSheet;

public class Torch extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void loadTransient() {
		handSprite = Assets.getSpriteSheet("items/holdingTorch", 16, 16);
		inventorySprite = Assets.getSpriteSheet("items/inventoryTorch", 11, 11);
	}
	
	public Torch() {
		super.name = "Torch";
		super.handSprite = Assets.getSpriteSheet("items/holdingTorch", 16, 16);
		super.inventorySprite = Assets.getSpriteSheet("items/inventoryTorch", 11, 11);
	}
	
	//public void draw(Graphics2D g, int dx, int dy) {
	//}
	
	//public void worldDraw(Graphics2D g, int xOff, int yOff) {
	//}
	
	int animFrame = 0;
	int frameCounter = 0;
	public void processPrimary(World world, Being weilder) {
		world.tiles[(int) (weilder.x/8)][(int) (weilder.y/8)-2].setLight(7);
		frameCounter++;
		if(frameCounter > 7) {
			frameCounter = 0;
			animFrame++;
			if(animFrame > 2) animFrame=0;
		}
	}
	

	public void drawInHand(Graphics2D g, int OffX, int OffY, Being being) {
		if(handSprite!=null) {
			int dx = being.dx-(being.sprite.spriteSizeX/2)-OffX;
			int dy = being.dy-(being.sprite.spriteSizeY-1)-OffY;
			dy = dy-(handSprite.spriteSizeY-16)/2;
			dx = dx-(handSprite.spriteSizeX-16)/2;
			handSprite.drawSprite(dx, dy, being.frame+(animFrame*3), being.lightLevel, being.facing, g);
		}
	}
	
}
