package blockoid.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import blockoid.Assets;
import blockoid.Game;
import blockoid.audio.Audio;
import blockoid.game.being.Being;
import blockoid.game.item.*;
import blockoid.game.object.GameObject;
import blockoid.game.tile.Dirt;
import blockoid.game.tile.Empty;
import blockoid.game.tile.Tile;
import blockoid.game.tile.Water;
import blockoid.graphics.SpriteSheet;
import blockoid.state.GameState;
import blockoid.state.State;

public class Player extends Being {	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public boolean inventoryOpen = false;
	public Inventory toolbelt;
	public int toolbeltIndex = 0;
	public int toolbeltIndexL = 1;
	public GameObject selectedObject = null;
	public Item hands = new Hands();
	
	public void loadTransient(Game game) {
		this.game = game;
		this.inventory.loadTransient();
		this.toolbelt.loadTransient();
		this.sprite = Assets.getSpriteSheet("characters/player", width, height);
		super.jump = Assets.getAudio("jump");
		super.hurt = Assets.getAudio("genericHurt");
	}
	
	public Player(Game game) {
		super(game);
		toolbelt = new ToolBelt("Tool Belt");
		maxDamage = 5;
		toolbelt.addItem(new PickAxe());
		toolbelt.addItem(new Torch());
		toolbelt.addItem(new DirtBlock(16));
		inventory.addItem(new WaterBlock(16));
		hitpool = 16*8;
		hitpoints = hitpool;
	}
	
	boolean oldSpace = false;
	public void act(World world, long elapsedTime) {
		//Controls
		boolean chatting = world.gameState.gui.chatting;
		if(!chatting) {
			if(game.keyboard.right){
				moveRight();
			}
			if(game.keyboard.left){
				moveLeft();
			}
	
			if (game.keyboard.j) {
				Being being = world.nearestBeing(this, false, false);
				if (being.distanceFrom(this) <= attackRange)
					world.fight(this, being);
			}
			
			if(game.keyboard.space && !oldSpace && timeOnGround>1 || game.keyboard.space && yVel < 0 && timeInAir < 14){
				jump();
			}
			oldSpace = game.keyboard.space;
		
		
		//Tool Belt
		toolbelt.moveTo(game.width/2 - toolbelt.sizeX/2, game.height - toolbelt.sizeY-2);
		if(game.mouseWheel.mouseWheelDown) {
			if(game.keyboard.shift) {
				toolbeltIndexL-=1;
				if(toolbeltIndexL < 0) toolbeltIndexL = toolbelt.slots.length-1;
			}else{
				toolbeltIndex-=1;
				if(toolbeltIndex < 0) toolbeltIndex = toolbelt.slots.length-1;
			}
		}
		if(game.mouseWheel.mouseWheelUp) {
			if(game.keyboard.shift) {
				toolbeltIndexL+=1;
				if(toolbeltIndexL > toolbelt.slots.length-1) toolbeltIndexL = 0;
			}else{
				toolbeltIndex+=1;
				if(toolbeltIndex > toolbelt.slots.length-1) toolbeltIndex = 0;
			}
		}
		for (int i = 1; i < 10; i++) {
			if (game.keyboard.num[i]) toolbeltIndex = i-1;
		}
		if(game.keyboard.num[0]) toolbeltIndex = 9;
		
		rightHandItem = toolbelt.slots[toolbeltIndex][0].item;
		leftHandItem = toolbelt.slots[toolbeltIndexL][0].item;
		//if(leftHandItem !=null && leftHandItem.isTwoHanded) {
		//	nullifyRH = true;
		//}else{nullifyRH = false;}
		//if(rightHandItem !=null && rightHandItem.isTwoHanded) {
		//	nullifyLH = true;
		//	nullifyRH = false;
		//}else{nullifyLH = false;}

		GameState ps = world.gameState;//(GameState)game.currentState();
		lastItemUse++;
		if(rightHandItem==null) rightHandItem = hands;
		if(ps.gui.selectedInventory==null && rightHandItem==hands) {
			if(ps.gui.grabbedItem==null) {
				if(world.game.mouse.holdL)
					emptyPrimary(world);
				if(world.game.mouse.holdR) 
					emptySecondary(world);
			}
		}
		if(inventoryOpen == false && rightHandItem!=null) {
			if(ps.gui.grabbedItem==null && !nullifyRH) {
				rightHandItem.processPrimary(world, this);
			}
		}
		if(inventoryOpen == false && leftHandItem!=null && !leftHandItem.equals(rightHandItem)) {
			if(ps.gui.grabbedItem==null && !nullifyLH) {
				leftHandItem.processPrimary(world, this);
			}
		}
		
		//Inventory
		if(ps.gui.inventories.size() > 1) {
			inventoryOpen = true;
		}else{inventoryOpen = false;}
		if(game.keyboard.i && oldi == false) {
			if(inventoryOpen==true) {
				ps.gui.removeInventory(inventory);
			}else{
				inventory.moveTo(game.width/2 - 4*12, game.height/2 - 2*12);
				ps.gui.addInventory(inventory);
			}
		}
		}
	}
	
	////Empty Hand////
	int lastItemUse = 0;
	
	public void emptyPrimary(World world) {
		//lastItemUse++;
		if(lastItemUse>=15) {
		if(selectedObject==null) {
			int tileX = (int) ((world.game.mouseMotion.x+world.CameraOffX)/8);
			int tileY = (int) ((world.game.mouseMotion.y+world.CameraOffY)/8);
			if(tileX >= world.sizeX) tileX = world.sizeX-1;
			if(tileX < 0) tileX = 0;
			if(tileY >= world.sizeY) tileY = world.sizeY-1;
			if(tileY < 0) tileY = 0;
	
			if(world.tiles[tileX][tileY].object!=null){
			world.tiles[tileX][tileY].object.damage(world, 1);
			}else{world.tiles[tileX][tileY].damage(world, 1);}
		}
		else {
			selectedObject.damage(world, 1);
			//selectedObject.
		}
		lastItemUse = 0;
		}
	}
	
	public void emptySecondary(World world) {
		
		if(lastItemUse>=15) {
		int tileX = (int) ((world.game.mouseMotion.x+world.CameraOffX)/8);
		int tileY = (int) ((world.game.mouseMotion.y+world.CameraOffY)/8);
		if(tileX >= world.sizeX) tileX = world.sizeX-1;
		if(tileX < 0) tileX = 0;
		if(tileY >= world.sizeY) tileY = world.sizeY-1;
		if(tileY < 0) tileY = 0;
	
		if(world.bgTiles[tileX][tileY].object!=null){
			world.bgTiles[tileX][tileY].object.damage(world, 1);
		}else{world.bgTiles[tileX][tileY].damage(world, 1);}
		
		lastItemUse = 0;
		}
	}
	
}
