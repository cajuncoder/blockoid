package blockoid.game.biome;

import java.util.Random;

import blockoid.game.World;
import blockoid.game.tile.Empty;
import blockoid.game.tile.Tile;

public class Cave {
	double xVec;
	double yVec;
	int length;
	int size;
	
	public Cave(World world, int startX, int startY) {
		Random r = new Random();
		length = r.nextInt(42)+32;
		size = r.nextInt(length/12)+1;
		xVec = r.nextDouble()*2-1;
		yVec = r.nextDouble()*2-1;
		Tile[] trail = new Tile[length];
		
		int x = startX;
		int y = startY;
		double dx = x;
		double dy = y;
		int counter = 0;
		for(int i = 0; i < length; i++) {
			
			if(counter > 3) {
				xVec+=r.nextDouble()-0.5;
				yVec+=r.nextDouble()-0.5;
				if(xVec > 1) xVec=1;
				if(yVec > 1) yVec=1;
				if(xVec < -1) xVec=-1;
				if(yVec < -1) yVec=-1;
				counter = 0;
			}
			counter++;
			
			trail[i] = world.tiles[x][y];
			dx += xVec;
			dy += yVec;
			x = (int) Math.round(dx);
			y = (int) Math.round(dy);
			if(x >= world.sizeX) break;
			if(y >= world.sizeY) break;
			if(x < 0) break;
			if(y < 0) break;
		}
		
		int tcounter = 0;
		for(int i = 0; i < trail.length; i++) {
			double step = (double)size/(double)length;
			Tile t = trail[i];
			if(t==null) break;
			world.tiles[t.xIndex][t.yIndex] = new Empty(t.xIndex,t.yIndex,false);
			
			int thisSize = 1;
			//if(i < length/2) {
			//	double m = (i+1.0)/(length/2);
			//	thisSize = (int) Math.round(m*size);
			//}
			//if(i >= length/2) {
			//	double m = (length-i)/(length);
			//	thisSize = (int) Math.round(m*size);
			//}
			
			//double m = (i+1.0)/(length);
			//thisSize = 1;//(int) Math.round(m*size);
			if(i < length/2) thisSize = (int)Math.round(step*2*i);
			if(i >= length/2) thisSize = (int)Math.round(step*2*(length-i));
			thisSize+=r.nextInt(2)+1;
			//thisSize = r.nextInt(thisSize/2+1)+thisSize/2;

			for(int hx = t.xIndex-(thisSize); hx < t.xIndex+(thisSize); hx++) {
				for(int hy = t.yIndex-(thisSize); hy < t.yIndex+(thisSize); hy++) {
					if(hx >= 0 && hx < world.sizeX) {
						if(hy >= 0 && hy < world.sizeY) {
							int xdist = Math.abs(world.tiles[hx][hy].xIndex - t.xIndex);
							int ydist = Math.abs(world.tiles[hx][hy].yIndex - t.yIndex);
							int dist = xdist+ydist;
							if(dist<thisSize) world.tiles[hx][hy] = new Empty(hx,hy,false);
						}
					}
				}
			}
			tcounter++;
		}
		
	} 
	
}
