package blockoid.game.object;

import blockoid.Assets;
import blockoid.game.item.Wood;
import blockoid.game.tile.Tile;

public class Chest extends GameObject {

	private static final long serialVersionUID = 1L;

	public void loadTransient() {
		spriteSheet =  Assets.getSpriteSheet("objects/chest", 10, 10);
		hit = Assets.getAudio("hitWood");
	}
	
	public Chest(Tile tile) {
		super(tile);
		spriteSheet = Assets.getSpriteSheet("objects/chest", 10, 10);
		dropItem = new Wood();
	}
	
	public GameObject getNewInstance(Tile tile) {
		GameObject o = new OakTree(tile);
		return o;
	}
}


