package blockoid.game.being.behavior;

import blockoid.game.being.Being;

public class FollowFriendly extends FollowBeing {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected String targetName() {
		return "target.friendly";
	}
}
