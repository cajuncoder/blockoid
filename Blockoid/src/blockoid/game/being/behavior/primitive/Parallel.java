package blockoid.game.being.behavior.primitive;

import blockoid.game.World;
import blockoid.game.being.Being;
import blockoid.game.being.behavior.Behavior;

public class Parallel extends CompositeBehavior {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Parallel(Behavior[] behaviors) {
		super(behaviors);
	}
	
	public int failed(Being being) {
		setState(being, Behavior.FAILED);
		return Behavior.FAILED;
	}
	
	public int running(Being being) {
		setState(being, Behavior.RUNNING);
		return Behavior.RUNNING;
	}
	
	public int succeeded(Being being) {
		setState(being, Behavior.SUCCEEDED);
		return Behavior.SUCCEEDED;
	}
	
	public int act(Being being, World world, long elapsedTime) {
		boolean failed = false;
		boolean succeeded = true;
		for (int i = 0; i < behaviors.length; i++) {
			Behavior behavior = behaviors[i];
			behavior.act(being, world, elapsedTime);
			if (behavior.hasFailed(being))
				failed = true;
			succeeded = succeeded && behavior.hasSucceeded(being);
		}
		
		if (failed)
			return failed(being);
		if (succeeded)
			return succeeded(being);
		return running(being);
	}
}
