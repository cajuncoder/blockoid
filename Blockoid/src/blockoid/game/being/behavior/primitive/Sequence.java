package blockoid.game.being.behavior.primitive;

import blockoid.game.World;
import blockoid.game.being.Being;
import blockoid.game.being.behavior.Behavior;

public class Sequence extends CompositeBehavior {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Sequence(Behavior[] behaviors) {
		super(behaviors);
	}
	
	public int act(Being being, World world, long elapsedTime) {
		Behavior behavior = null;
		int i = 0;
		for (i = 0; i < behaviors.length; i++) {
			behavior = behaviors[i];
			if (!behavior.isRunning(being)) {
				if (behavior.hasFailed(being))
					return failed(being);
				if (behavior.hasSucceeded(being))
					continue;
			}
	
			behavior.act(being, world, elapsedTime);
			if (behavior.isRunning(being))
				return running(being);
			if (behavior.hasFailed(being))
				return failed(being);
			break;
		}
		
		if (i == behaviors.length-1 && behavior.hasSucceeded(being))
			return succeeded(being);
		
		return getState(being);
	}
}
