package blockoid.game.being.behavior;

import java.io.Serializable;
import java.util.HashMap;

public class BehaviorCache implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected HashMap<String, Object> cache;
	
	public BehaviorCache() {
		cache = new HashMap<String, Object>();
	}
	
	public void set(String key, Object val) {
		cache.put(key, val);
	}
	
	public Object get(String key) {
		return cache.get(key);
	}
}
