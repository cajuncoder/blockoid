package blockoid.game.being.behavior;

import blockoid.game.being.Being;

public class FollowEnemy extends FollowBeing {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected String targetName() {
		return "target.enemy";
	}
}
