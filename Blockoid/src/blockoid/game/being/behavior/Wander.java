package blockoid.game.being.behavior;

import blockoid.Game;
import blockoid.game.World;
import blockoid.game.being.Being;
import blockoid.game.being.Memory;

public class Wander extends Move {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Wander() {
		super();
	}

	double oldX = 0;
	int stuckCounter = 0;
	@Override
	public int act(Being being, World world, long elapsedTime) {
		Memory targetX = being.brain.getMemory("target.x");
		
		//check if stuck
		boolean stuck = false;
		if(isRunning(being) && oldX == being.x) {
			stuckCounter++;
		}else{stuckCounter=0;}
		if(stuckCounter > 40) stuck=true;
		
		if (!isRunning(being) || targetX == null || stuck) {
			int delta = (int)(Math.random() * (being.wanderRange + 1));
			if (Math.random() < 0.5) delta = -delta;
			int x = (int) (being.x + delta);
			being.brain.addMemory("target.x", new Memory<Integer>(x));
		}

		oldX = being.x;
		return super.act(being, world, elapsedTime);
	}

	@Override
	protected void setSpeed(Being being) {
		being.useMinSpeed();
	}
	
}
