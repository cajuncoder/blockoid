package blockoid.game.being;

import java.io.Serializable;

public class Memory<T> implements Serializable {	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	T memory;
	public Memory(T mem) {
		memory = mem;
	}
	public T retrieve() {
		return memory;
	}
}
